%define javaver        1.8.0
%define priority       18000
%define gurlhandler    /desktop/gnome/url-handlers
%define jnlphandler    %{gurlhandler}/jnlp
%define jnlpshandler   %{gurlhandler}/jnlps
%define javadir        %{_jvmdir}/java-%{javaver}-openjdk
%define jredir         %{_jvmdir}/jre-%{javaver}-openjdk
%define binsuffix      .itweb
%define preffered_java java-%{javaver}-openjdk
%global debug_package  %{nil}
Name:                icedtea-web
Version:             1.8.4
Release:             5
Summary:	     Additional Java components for OpenJDK - Java Web Start implementation
License:             LGPLv2+ and GPLv2 with exceptions
URL:                 http://icedtea.classpath.org/wiki/IcedTea-Web
Source0:             http://github.com/AdoptOpenJDK/IcedTea-Web/archive/%{name}-%{version}.tar.gz

Patch0006:           fix-for-cargo-test-sometimes-fails-when-make-runs-with-several-simultaneous-jobs.patch
Patch0007:           add-dunce-lib.patch

BuildRequires:       javapackages-tools javapackages-local %{preffered_java}-devel
BuildRequires:       desktop-file-utils glib2-devel autoconf automake 	cargo junit hamcrest
BuildRequires:       libappstream-glib tagsoup git
Requires:            %{preffered_java} javapackages-tools  tagsoup
Recommends:          bash-completion

Requires(post):      chkconfig >= 1.7 GConf2
Requires(post):      javapackages-tools %{_sbindir}/alternatives

Requires(postun):    chkconfig >= 1.7 GConf2
Requires(postun):    javapackages-tools %{_sbindir}/alternatives

Provides:            javaws = 1:%{javaver}
Provides:            %{preffered_java}-javaws = 1:%{version}

%description
The IcedTea-Web project provides a an implementation of Java Web Start(originally based on the Netx project, now opensource part of OpenWebStart) and a settings tool to manage deployment settings for the aforementioned Web Start implements.

%package devel
Summary:             Header files for icedtea-web
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch

%description devel
Header files for icedtea-web.

%package help
Summary:             Help documents for icedtea-web
Requires:            %{name} = %{version}-%{release}
Provides:            %{name}-javadoc = %{version}-%{release}
Obsoletes:           %{name}-javadoc < %{version}-%{release}
BuildArch:           noarch

%description help
Help documents for icedtea-web.

%prep
%autosetup -n %{name}-%{version} -p1 -S git

%build
autoreconf -vfi
CXXFLAGS="$RPM_OPT_FLAGS $RPM_LD_FLAGS" \
%configure \
    --with-pkgversion=fedora-%{release}-%{_arch} \
    --docdir=%{_datadir}/javadoc/%{name} \
    --with-jdk-home=%{javadir} \
    --with-jre-home=%{jredir} \
    --libdir=%{_libdir} \
    --program-suffix=%{binsuffix} \
    --disable-native-plugin \
    --with-itw-libs=DISTRIBUTION \
    --with-modularjdk-file=%{_sysconfdir}/java/%{name}    \
    --prefix=%{_prefix}
%make_build

%install
%make_install

install -d %{buildroot}%{_sysconfdir}/bash_completion.d/
mv completion/policyeditor.bash %{buildroot}%{_sysconfdir}/bash_completion.d/
mv completion/javaws.bash %{buildroot}%{_sysconfdir}/bash_completion.d/
mv completion/itweb-settings.bash %{buildroot}%{_sysconfdir}/bash_completion.d/
mv %{buildroot}/%{_mandir}/man1/javaws.1 %{buildroot}/%{_mandir}/man1/javaws.itweb.1
install -d -m 755 %{buildroot}%{_datadir}/{applications,pixmaps}
desktop-file-install --vendor ''\
  --dir %{buildroot}%{_datadir}/applications javaws.desktop
desktop-file-install --vendor ''\
  --dir %{buildroot}%{_datadir}/applications itweb-settings.desktop
desktop-file-install --vendor ''\
  --dir %{buildroot}%{_datadir}/applications policyeditor.desktop

DESTDIR=%{buildroot} appstream-util install metadata/icedtea-web.metainfo.xml
DESTDIR=%{buildroot} appstream-util install metadata/icedtea-web-javaws.appdata.xml

install -d %{buildroot}%{_javadir}
cd %{buildroot}%{_javadir}
ln -s ../icedtea-web/javaws.jar icedtea-web.jar
ln -s ../icedtea-web/plugin.jar icedtea-web-plugin.jar
cd -

install -d %{buildroot}/%{_mavenpomdir}
cp metadata/icedtea-web.pom %{buildroot}/%{_mavenpomdir}/icedtea-web.pom
cp metadata/icedtea-web-plugin.pom %{buildroot}/%{_mavenpomdir}/icedtea-web-plugin.pom

%add_maven_depmap icedtea-web.pom icedtea-web.jar
%add_maven_depmap icedtea-web-plugin.pom icedtea-web-plugin.jar

cp  netx.build/lib/src.zip %{buildroot}%{_datadir}/icedtea-web/javaws.src.zip
cp liveconnect/lib/src.zip %{buildroot}%{_datadir}/icedtea-web/plugin.src.zip
%find_lang icedtea-web --all-name --with-man

%check
make check
appstream-util validate %{buildroot}/%{_datadir}/appdata/*.xml || :

%post
alternatives \
  --install %{_bindir}/javaws javaws.%{_arch} %{_prefix}/bin/javaws%{binsuffix} %{priority} \
  --family  %{preffered_java}.%{_arch} \
  --slave   %{_bindir}/itweb-settings itweb-settings %{_prefix}/bin/itweb-settings%{binsuffix} \
  --slave   %{_bindir}/policyeditor policyeditor %{_prefix}/bin/policyeditor%{binsuffix} \
  --slave   %{_bindir}/ControlPanel	ControlPanel %{_prefix}/bin/itweb-settings%{binsuffix} \
  --slave   %{_mandir}/man1/javaws.1.gz	javaws.1.gz	%{_mandir}/man1/javaws%{binsuffix}.1.gz \
  --slave   %{_mandir}/man1/ControlPanel.1.gz ControlPanel.1.gz	%{_mandir}/man1/itweb-settings.1.gz

gconftool-2 -s  %{jnlphandler}/command  '%{_prefix}/bin/javaws%{binsuffix} %s' --type String &> /dev/null || :
gconftool-2 -s  %{jnlphandler}/enabled  --type Boolean true &> /dev/null || :
gconftool-2 -s %{jnlpshandler}/command '%{_prefix}/bin/javaws%{binsuffix} %s' --type String &> /dev/null || :
gconftool-2 -s %{jnlpshandler}/enabled --type Boolean true &> /dev/null || :

%posttrans
update-desktop-database &> /dev/null || :
exit 0

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ]
then
  alternatives --remove javaws %{_prefix}/bin/javaws%{binsuffix}
  gconftool-2 -u  %{jnlphandler}/command &> /dev/null || :
  gconftool-2 -u  %{jnlphandler}/enabled &> /dev/null || :
  gconftool-2 -u %{jnlpshandler}/command &> /dev/null || :
  gconftool-2 -u %{jnlpshandler}/enabled &> /dev/null || :
fi
exit 0

%files -f .mfiles -f icedtea-web.lang
%license COPYING
%{_sysconfdir}/bash_completion.d/*
%config(noreplace) %{_sysconfdir}/java/icedtea-web/itw-modularjdk.args
%{_prefix}/bin/*
%{_datadir}/applications/*
%{_datadir}/icedtea-web/*.jar
%{_datadir}/icedtea-web/*.png
%{_datadir}/pixmaps/*
%{_datadir}/appdata/*.xml

%files devel
%license COPYING
%{_datadir}/icedtea-web/*.zip

%files help
%doc COPYING NEWS README
%{_datadir}/javadoc/icedtea-web
%{_datadir}/man/man1/*

%changelog
* Fri Feb 5 2021 jdkboy <ge.guo@huawei.com> - 1.8.4-5
- add add-dunce-lib.patch to avoid accssing to Cargo

* Mon Dec 21 2020 noah <hedongbo@huawei.com> - 1.8.4-4
- add a license to this repo

* Tue Oct 20 2020 noah <hedongbo@huawei.com> - 1.8.4-3
- add fix-for-cargo-test-sometimes-fails-when-make-runs-with-several-simultaneous-jobs.patch

* Sat Aug 29 2020 noah <hedongbo@huawei.com> - 1.8.4-2
- Changed source url and description

* Thu Aug 27 2020 noah <hedongbo@huawei.com> - 1.8.4-1
- Update to 1.8.4

* Tue Aug 25 2020 lingsheng <lingsheng@huawei.com> - 1.8-4
- Fix accidental build fail in make multithreading

* Mon Jun 8 2020 yaokai13 <yaokai13@huawei.com> - 1.8-3
- Update to 1.8

* Fri Feb 14 2020 wangzhishun1<wangzhishun1@huawei.com> - 1.7.1-11
- Package init
